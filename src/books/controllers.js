const Router = require('express').Router();

const {
    ServerError
} = require('../errors');

const {
    getAll,
    getById,
    getAllForUser,
    add,
    update,
    remove,
    removeForUser
} = require('./services.js');

const {
    authorizeUser
} = require('../authorization');

const {
    ADMIN_ROLE,
    READER_ROLE
} = process.env;

Router.get('/user', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId
    } = req.state;

    const books = await getAllForUser(userId);

    res.json(books);
});

Router.get('/:id', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    const book = await getById(id);

    res.json(book);

});

Router.get('/', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {

    const books = await getAll();

    res.json(books);

});

Router.post('/', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        nume,
        genre,
        id_autor
    } = req.body;

    const {
        userId
    } = req.state;

    if (typeof nume !== 'string') {
        throw new ServerError('Nume invalid!', 400);
    }
    
    if (typeof genre !== 'string') {
        throw new ServerError('Gen invalid!', 400);
    }

    if (typeof id_autor !== 'number') {
        throw new ServerError('Id invalid!', 400);
    }

    const id = await add(nume, genre, id_autor, userId);

    res.json({id, nume, genre, id_autor, userId});
});

Router.put('/:id', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    const {
        nume,
        genre,
        id_autor
    } = req.body;

    if (typeof nume !== 'string') {
        throw new ServerError('Nume invalid!', 400);
    }
    
    if (typeof genre !== 'string') {
        throw new ServerError('Gen invalid!', 400);
    }

    if (typeof id_autor !== 'number') {
        throw new ServerError('Id invalid!', 400);
    }

    await update(id, nume, genre, id_autor);

    res.json({id, nume, genre, id_autor});

});

Router.delete('/:id', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    await remove(id);

    res.status(200).end();
});

Router.delete('/user', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId
    } = req.state;

    await removeForUser(userId);

    res.status(200).end();
});

/* 
 * ruta de sincronizare, nu va fi apelata direct de catre frontend niciodata
 * in laboratoarele urmatoare vom invata sa protejam rutele "publice" folosind kong
 */

Router.delete('/user/:id', async (req, res) => {
    const {
        id
    } = req.params;

    await removeForUser(id);

    res.status(200).end();
});

module.exports = Router;