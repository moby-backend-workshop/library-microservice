const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const getAll = async () => {
    console.info('Getting all authors...');

    const authors = await query('SELECT * FROM authors');

    return authors;
};

const getById = async (id) => {
    console.info(`Getting author ${id}...`);

    const authors = await query('SELECT * FROM authors WHERE id=$1', [id]);

    if (authors.length !== 1) {
        throw new ServerError('Author does not exist!', 404);
    }

    return authors[0];
};

const add = async (nume) => {
    console.info(`Adaug un autor cu numele ${nume}`);

    try {
        const authors = await query('INSERT INTO authors (nume) VALUES ($1) RETURNING id', [nume]);
        return authors[0].id;
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Author already exists!', 409);
        }
        throw e;
    }
};

const update = async (id, nume) => {
    console.info(`Modific numele autorului ${id} in ${nume}`);
    try {
        await query('UPDATE authors SET nume=$1 WHERE id=$2', [nume, id]);
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Author already exists!', 409);
        }
        throw e;
    }
};

const remove = async (id) => {
    console.info(`Sterg autorul ${id}...`);

    await query('DELETE FROM authors WHERE id=$1', [id]);
}

module.exports = {
    getAll,
    getById,
    add,
    update,
    remove
}